import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeaveComponent } from './pages/leave/leave.component';
import { LoginComponent } from './pages/login/login.component';
import { MyLeaveComponent } from './pages/my-leave/my-leave.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'leave',
    component: LeaveComponent,
  },
  {
    path: 'my-leave',
    component: MyLeaveComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
